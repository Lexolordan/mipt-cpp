#pragma once

#include <memory>

namespace mipt {
    
// Your code goes here

// template<size_t N>
// class StackStorage {
//     // Your code goes here
// };

// template<typename T, size_t N>
// class StackAllocator {
//     // Your code goes here
// };

template <typename T, size_t N>
using StackAllocator = std::allocator<T>;

} // namespace mipt