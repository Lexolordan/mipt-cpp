project(list-allocator)

include(../cmake/GoogleTest.cmake)

add_library(mipt-hashmap-allocator allocator.cpp)
add_library(mipt-hashmap-list list.cpp)
target_link_libraries(mipt-hashmap-list PUBLIC mipt-hashmap-allocator)

add_library(mipt-hashmap unordered_map.cpp)
target_link_libraries(mipt-hashmap PUBLIC mipt-hashmap-list)

add_mipt_test(mipt-hashmap-tests tests.cpp)
target_link_libraries(mipt-hashmap-tests PUBLIC mipt-hashmap)
