#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "unordered_map.h"

#include <vector>
#include <string>
#include <iterator>
#include <cassert>

#include <iostream>

using namespace mipt;

TEST(TestUnorderedMap, test_basics) {
    UnorderedMap<std::string, int> m;

    m["aaaaa"] = 5;
    m["bbb"] = 6;
    m.at("bbb") = 7;

    ASSERT_EQ(m.size(), 2);
    ASSERT_EQ(m["aaaaa"], 5);
    ASSERT_EQ(m["bbb"], 7);
    ASSERT_EQ(m["ccc"], 0);
    ASSERT_EQ(m.size(), 3);
   
    try {
        m.at("xxxxxxxx");
        ASSERT_TRUE(false);
    } catch (...) {
    }
    
    auto it = m.find("dddd");
    ASSERT_EQ(it, m.end());

    it = m.find("bbb");
    ASSERT_EQ(it->second, 7);

    ++it->second;
    ASSERT_EQ(it->second, 8);

    for (auto& item : m) {
        --item.second;
    }
    ASSERT_EQ(m.at("aaaaa"), 4);

    {
        auto mm = m;
        m = std::move(mm);
    }
    
    auto res = m.emplace("abcde", 2);
    ASSERT_TRUE(res.second);
}
