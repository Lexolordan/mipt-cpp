#include <unordered_map>

namespace mipt {

template<typename Key, typename Value>
using UnorderedMap = std::unordered_map<Key, Value>;

}