project(list-allocator)

include(../cmake/GoogleTest.cmake)

add_library(mipt-allocator allocator.cpp)
add_library(mipt-list list.cpp)
target_link_libraries(mipt-list PUBLIC mipt-allocator)

add_mipt_test(mipt-list-allocator-tests tests.cpp)
target_link_libraries(mipt-list-allocator-tests PUBLIC mipt-list)

# set stack size to 210MB
if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(SPECIFIC_FLAGS "-Wl,-stack_size -Wl,0xd200000")
else()
    set(SPECIFIC_FLAGS "-Wl,--stack,220200960")
endif()

set_target_properties(mipt-list-allocator-tests PROPERTIES LINK_FLAGS ${SPECIFIC_FLAGS})
