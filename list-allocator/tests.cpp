#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "list.h"

using namespace mipt;

TEST(TestList, test_basics) {
    List<int> test_list;

    ASSERT_EQ(0, test_list.size());
    test_list.push_back(1);
    test_list.push_back(2);
    test_list.push_back(3);
    ASSERT_EQ(3, test_list.size());

    auto test_list2 = test_list;
    ASSERT_EQ(3, test_list2.size());
    test_list2.pop_back();
    ASSERT_EQ(2, test_list2.size());

    ASSERT_EQ(3, test_list.size());
}
