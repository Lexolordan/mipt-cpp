find_package(GTest REQUIRED CONFIG)
include(GoogleTest)

set(CMAKE_CXX_FLAGS  "--coverage")

function(add_mipt_test target sources)
    add_executable(${target} ${sources})
    target_link_libraries(${target} PUBLIC GTest::gtest GTest::gtest_main)
    target_link_libraries(${target} PUBLIC GTest::gmock GTest::gmock_main)
    gtest_discover_tests(${target})
endfunction()
